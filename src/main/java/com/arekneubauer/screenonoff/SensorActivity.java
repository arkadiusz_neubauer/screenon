package com.arekneubauer.screenonoff;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.arekneubauer.common.Globals;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.util.Calendar;
import java.util.Date;

public class SensorActivity extends Activity {
    private ProgressBar pbProximity;
    private Globals globals;
    private boolean isCalibrating=false;
    private int calibrationTriggerCount=0;
    private AdView mAdView;

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action != null) {
                if (action.equals("UPDATE_SENSOR")) {
                    float distance = intent.getFloatExtra("sensor", 0);
                    UpdateSensor(distance);
                }
                if (action.equals("INIT_SENSOR")) {
                    float max = intent.getFloatExtra("max", 0);
                    String name = intent.getStringExtra("name");
                    String vendor = intent.getStringExtra("vendor");
                    InitSensor(max, vendor, name);
                }
                if (action.equals("UPDATE_CALIBRATION")) {
                    //int triggerCount = intent.getIntExtra("count", 0);
                    calibrationTriggerCount++;
                    Calendar now = Calendar.getInstance();
                    now.setTime(new Date());
                    long triggerDelay = now.getTimeInMillis() - Globals.then.getTimeInMillis();
                    globals.setTriggerCount(calibrationTriggerCount);
                    globals.setTriggerDelay(triggerDelay);
                    Refresh();
                }
            }
        }
    };

    // ---------------------------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_sensor);

        globals = new Globals(this);

        globals.setIsCalibrating(false);

        IntentFilter filterSend = new IntentFilter();
        filterSend.addAction("UPDATE_SENSOR");
        filterSend.addAction("INIT_SENSOR");
        filterSend.addAction("UPDATE_CALIBRATION");
        registerReceiver(receiver, filterSend);

        final Intent intent = new Intent(getBaseContext(), SensorService.class);
        //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startService(intent);

        pbProximity = findViewById(R.id.pbProximity);

        TextView tvManual = findViewById(R.id.tvManual);
        tvManual.setText(getText(R.string.proximity_manual));

        TextView tvVersion = findViewById(R.id.tvVersion);

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            tvVersion.setText(String.format("%s v%s", getText(R.string.app_name),pInfo.versionName ));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        MobileAds.initialize(this, "ca-app-pub-6436201726627350~7679411524");
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mAdView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                ScrollView container = findViewById(R.id.scrollView);
                TextView tvVersion = findViewById(R.id.tvVersion);

                RelativeLayout.LayoutParams tvVersionLayout = (RelativeLayout.LayoutParams) tvVersion.getLayoutParams();
                tvVersionLayout.bottomMargin = mAdView.getHeight() + 20;
                tvVersion.setLayoutParams(tvVersionLayout);

                RelativeLayout.LayoutParams containerLayout = (RelativeLayout.LayoutParams) container.getLayoutParams();
                containerLayout.bottomMargin = tvVersion.getHeight();
                container.setLayoutParams(containerLayout);
            }
        });
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                mAdView.setVisibility(View.VISIBLE);
            }
        });
        // -----------------------------------------------------------------------------------------

        Button btnCalibrate = findViewById(R.id.btnCalibrate);
        btnCalibrate.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                isCalibrating = !isCalibrating;
                globals.setIsCalibrating(isCalibrating);
                Button btn = v.findViewById(R.id.btnCalibrate);
                if (isCalibrating) {
                    Globals.triggerCount = 0;
                    Globals.triggerDelay = 0;
                    Globals.then.setTime(new Date());
                    btn.setText(R.string.button_stop);
                    TextView tvSettings = findViewById(R.id.tvCurrentSettings);
                    tvSettings.setText(getText(R.string.trigger_learn));
                } else {
                    calibrationTriggerCount = 0;
                    btn.setText(R.string.button_calibrate);
                    Refresh();
                }
            }
        });
        // -----------------------------------------------------------------------------------------

        Button btnDefaults = findViewById(R.id.btnDefaults);
        btnDefaults.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                isCalibrating = false;
                globals.setTriggerCount(Globals.DEFAULT_TRIGGER_COUNT);
                globals.setTriggerDelay(Globals.DEFAULT_TRIGGER_DELAY);
                globals.setIsCalibrating(isCalibrating);
                Refresh();
            }
        });
        // -----------------------------------------------------------------------------------------

        Button btnClose = findViewById(R.id.btnClose);
        btnClose.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });
        // -----------------------------------------------------------------------------------------

        CheckBox chkKeyboardUnlock = findViewById(R.id.chkKeyboardUnlock);
        chkKeyboardUnlock.setChecked(globals.getKeyboardLock());
        chkKeyboardUnlock.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                try {
                    globals.setKeyboardLock(isChecked);
                } catch (Exception e) {
                    Log.e("onKeyLockChecked", e.getMessage());
                    e.printStackTrace();
                }
            }
        });

        // -----------------------------------------------------------------------------------------

        CheckBox chkPlaySound = findViewById(R.id.chkPlaySound);
        chkPlaySound.setChecked(globals.getTickSound());
        chkPlaySound.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                try {
                    globals.setTickSound(isChecked);
                    Refresh();
                } catch (Exception e) {
                    Log.e("onPlaySoundChecked", e.getMessage());
                    e.printStackTrace();
                }
            }
        });

        // -----------------------------------------------------------------------------------------

        CheckBox chkAutoStart = findViewById(R.id.chkAutoStart);
        chkAutoStart.setChecked(globals.getStartOnBoot());
        chkAutoStart.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                try {
                    globals.setStartOnBoot(isChecked);
                    Refresh();
                } catch (Exception e) {
                    Log.e("onAutoStartChecked", e.getMessage());
                    e.printStackTrace();
                }
            }
        });

        // -----------------------------------------------------------------------------------------

        Refresh();
    }
    // ---------------------------------------------------------------------------------------------

    private void Refresh() {
        TextView tvCurrentSettings = findViewById(R.id.tvCurrentSettings);

        float delay = globals.getTriggerDelay();
        delay = delay/1000;

        String cSettings = getText(R.string.current_settings)
                .toString()
                .replace("{1}", ""+globals.getTriggerCount())
                .replace("{2}", ""+ delay);

        tvCurrentSettings.setText(cSettings);
    }

    // ---------------------------------------------------------------------------------------------

    private void UpdateSensor(float distance) {
        int pProgress = Math.round(distance*10);
        pbProximity.setProgress(pProgress);

        TextView tvSensorMonitor = findViewById(R.id.tvSensorMonitor);

        tvSensorMonitor.setText(String.format("%s %.2f", getText(R.string.sensor_status),  distance));
    }

    // ---------------------------------------------------------------------------------------------

    private void InitSensor(float max, String vendor, String name) {
        int pMax = Math.round(max*10);
        pbProximity.setMax(pMax);

        TextView tvProximityName = findViewById(R.id.tvProximityName);
        tvProximityName.setText(String.format("%s %s", getText(R.string.sensor_name),  name));

        TextView tvProximityVendor =  findViewById(R.id.tvProximityVendor);
        tvProximityVendor.setText(String.format("%s %s", getText(R.string.sensor_vendor), vendor));
    }

    // ---------------------------------------------------------------------------------------------

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    // ---------------------------------------------------------------------------------------------
}


