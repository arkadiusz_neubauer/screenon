package com.arekneubauer.screenonoff;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.arekneubauer.common.Globals;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.util.Calendar;
import java.util.Date;

public class SensorService extends Service implements SensorEventListener {
    //private NotificationManager mNM;
    private SensorManager mSensorManager;
    private Sensor mProximity;
    private int pSensorTriggerCount = 0;
    private ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, (int) (ToneGenerator.MAX_VOLUME * 0.85));
    private Calendar then = Calendar.getInstance();
    private Globals globals;
    // Unique Identification Number for the Notification.
    // We use it on Notification start, and to cancel it.
    private int NOTIFICATION = R.string.local_service_started;

    /**
     * Class for clients to access.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with
     * IPC.
     */
    public class LocalBinder extends Binder {
        SensorService getService() {
            Refresh();
            return SensorService.this;
        }
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        Log.i("LocalService", "Received start id " + startId + ": " + intent);
        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, SensorActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle(getText(R.string.app_name))
                .setTicker(getText(R.string.app_name))
                .setContentText("")
                .setSmallIcon(R.mipmap.ic_phone_lock)
                        //.setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                .setContentIntent(contentIntent)
                .setOngoing(true).build();
        startForeground(NOTIFICATION, notification);

        Refresh();
        return START_STICKY;
    }

    // ---------------------------------------------------------------------------------------------

    @Override
    public void onCreate() {
        super.onCreate();
        globals = new Globals(this);
        //mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        if (mSensorManager != null) {
            mProximity = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
            mSensorManager.registerListener(SensorService.this, mProximity, SensorManager.SENSOR_DELAY_NORMAL);
        }

        //showNotification("");
    }

    // ---------------------------------------------------------------------------------------------

    public void Refresh() {
        if (mProximity != null) {
            Intent intentBroadcast = new Intent("INIT_SENSOR");
            intentBroadcast.putExtra("name", mProximity.getName());
            intentBroadcast.putExtra("vendor", mProximity.getVendor());
            intentBroadcast.putExtra("max", mProximity.getMaximumRange());
            sendBroadcast(intentBroadcast);
            UpdateProximitySensor(mProximity.getMaximumRange());
        }
    }

    // ---------------------------------------------------------------------------------------------

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Cancel the persistent notification.
        //mNM.cancel(NOTIFICATION);
        toneG.release();
        mSensorManager.unregisterListener(SensorService.this);
    }

    // ---------------------------------------------------------------------------------------------

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    // ---------------------------------------------------------------------------------------------

    // This is the object that receives interactions from clients.  See
    // RemoteService for a more complete example.
    private final IBinder mBinder = new LocalBinder();

    // ---------------------------------------------------------------------------------------------

    /*
    private void showNotification(CharSequence text) {
        if (mNM != null) {
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.mipmap.ic_phone_lock)
                            .setContentTitle(getText(R.string.app_name))
                            .setContentText(text)
                            .setOngoing(true);

            PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                    new Intent(this, SensorActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);

            mBuilder.setContentIntent(contentIntent);
            mNM.notify(NOTIFICATION, mBuilder.build());
        }
    }
    */
    // ---------------------------------------------------------------------------------------------

    @Override
    public final void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Do something here if sensor accuracy changes.
    }

    // ---------------------------------------------------------------------------------------------

    @Override
    public final void onSensorChanged(SensorEvent event) {
        float distance = event.values[0];
        //if (distance == mProximity.getMaximumRange()) {
        if (distance == .0) {
            if (globals.isIsCalibrating()) {
                UpdateCalibration();
                toneG.startTone(ToneGenerator.TONE_PROP_BEEP, 60);
            } else {
                TryScreenOn();
            }
        }
        UpdateProximitySensor(distance);
    }

    //----------------------------------------------------------------------------------------------

    private void UpdateCalibration() {
        Intent intentBroadcast = new Intent("UPDATE_CALIBRATION");
        sendBroadcast(intentBroadcast);
    }

    //----------------------------------------------------------------------------------------------

    private void UpdateProximitySensor(float distance) {
        Intent intentBroadcast = new Intent("UPDATE_SENSOR");
        intentBroadcast.putExtra("sensor", distance);
        sendBroadcast(intentBroadcast);
    }

    //----------------------------------------------------------------------------------------------

    private void TryScreenOn() {
        Calendar now = Calendar.getInstance();
        now.setTime(new Date());

        //long nowMs = now.getTimeInMillis();
        //long thenMs = then.getTimeInMillis();
        long divMs = now.getTimeInMillis() - then.getTimeInMillis();

        if (pSensorTriggerCount == 0) {
            then.setTime(new Date());
            pSensorTriggerCount++;
        } else if (divMs <= globals.getTriggerDelay()) {
            //then.setTime(new Date());
            pSensorTriggerCount++;
        } else {
            pSensorTriggerCount = 0;
        }

        if (pSensorTriggerCount == globals.getTriggerCount()) {
            pSensorTriggerCount = 0;

            unlockScreen();
            if (globals.getTickSound()) {
                toneG.startTone(ToneGenerator.TONE_PROP_BEEP, 60);
            }
            //showNotification(getText(R.string.wakeup_action));
            Toast.makeText(this, getText(R.string.wakeup_action), Toast.LENGTH_SHORT).show();

        }
    }

    //----------------------------------------------------------------------------------------------

    private void unlockScreen() {
        PowerManager mPowerManager = (PowerManager) getSystemService(POWER_SERVICE);
        PowerManager.WakeLock mWakeLock = mPowerManager.newWakeLock(
                //PowerManager.FULL_WAKE_LOCK |
                PowerManager.SCREEN_BRIGHT_WAKE_LOCK |
                PowerManager.ACQUIRE_CAUSES_WAKEUP,
                getClass()
                        .getName());
        mWakeLock.acquire(500);
        //mWakeLock.release();
    }

    //----------------------------------------------------------------------------------------------



}