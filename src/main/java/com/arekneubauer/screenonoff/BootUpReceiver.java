package com.arekneubauer.screenonoff;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.arekneubauer.common.Globals;

public class BootUpReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Globals g = new Globals(context);
        if (g.getStartOnBoot()) {
            Intent i = new Intent(context, SensorService.class);
            context.startService(i);
        }
    }
}