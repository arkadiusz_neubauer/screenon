package com.arekneubauer.common;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Calendar;

/**
 * Created by m342769 on 2015-05-08.
 */
public class Globals {
    public static int triggerCount=0;
    public static int triggerDelay=0;

    public static Calendar then = Calendar.getInstance();

    private static final String PREFERENCES_FILE = "ScreenOn790626";

    private static final String TRIGGER_DELAY_PARAM = "proximity_trigger_delay";
    private static final String TRIGGER_COUNT_PARAM = "proximity_trigger_count";
    private static final String IS_CALIBRATING_PARAM = "is_calibrating";
    private static final String KEYBOARD_LOCK = "keyboard_lock";
    private static final String TICK_SOUND = "tick_sound";
    private static final String START_ON_BOOT = "start_on_boot";

    public static final int DEFAULT_TRIGGER_DELAY = 1400;
    public static final int DEFAULT_TRIGGER_COUNT = 2;

    private Context mContext;

    //----------------------------------------------------------------------------------------------

    public Globals(Context context) {
        this.mContext = context;
    }

    private SharedPreferences getSharedPreferences() {
        return mContext.getSharedPreferences(PREFERENCES_FILE, Context.MODE_MULTI_PROCESS);
    }
    //----------------------------------------------------------------------------------------------

    public boolean getStartOnBoot() {
        SharedPreferences settings = getSharedPreferences();
        return settings.getBoolean(START_ON_BOOT, true);
    }

    public void setStartOnBoot(boolean startOnBoot) {
        SharedPreferences settings = getSharedPreferences();
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(START_ON_BOOT, startOnBoot);
        editor.apply();
    }

    //----------------------------------------------------------------------------------------------

    public boolean isIsCalibrating() {
        SharedPreferences settings = getSharedPreferences();
        return settings.getBoolean(IS_CALIBRATING_PARAM, false);

    }

    public void setIsCalibrating(boolean isCalibrating) {
        SharedPreferences settings = getSharedPreferences();
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(IS_CALIBRATING_PARAM, isCalibrating);
        editor.apply();
    }

    //----------------------------------------------------------------------------------------------

    public long getTriggerDelay() {
        SharedPreferences settings = getSharedPreferences();
        return settings.getLong(TRIGGER_DELAY_PARAM, DEFAULT_TRIGGER_DELAY);
    }

    public void setTriggerDelay(long tDelay) {
        SharedPreferences settings = getSharedPreferences();
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong(TRIGGER_DELAY_PARAM, tDelay);
        editor.apply();
    }

    //----------------------------------------------------------------------------------------------

    public int getTriggerCount() {
        SharedPreferences settings = getSharedPreferences();
        return settings.getInt(TRIGGER_COUNT_PARAM, DEFAULT_TRIGGER_COUNT);
    }

    public void setTriggerCount(int tCount) {
        SharedPreferences settings = getSharedPreferences();
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(TRIGGER_COUNT_PARAM, tCount);
        editor.apply();
    }

    //----------------------------------------------------------------------------------------------

    public boolean getTickSound() {
        SharedPreferences settings = getSharedPreferences();
        return settings.getBoolean(TICK_SOUND, false);
    }

    public void setTickSound(boolean tickSound) {
        SharedPreferences settings = getSharedPreferences();
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(TICK_SOUND, tickSound);
        editor.apply();
    }

    //----------------------------------------------------------------------------------------------

    public boolean getKeyboardLock() {
        SharedPreferences settings = getSharedPreferences();
        return settings.getBoolean(KEYBOARD_LOCK, false);
    }

    public void setKeyboardLock(boolean keyboardLock) {
        SharedPreferences settings = getSharedPreferences();
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(KEYBOARD_LOCK, keyboardLock);
        editor.apply();
    }

    //----------------------------------------------------------------------------------------------
}
